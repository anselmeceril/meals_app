import 'package:flutter/material.dart';

class Categorie {
  final String id;
  final String title;
  final Color color;

  Categorie({required this.id, required this.title, required this.color});
}
