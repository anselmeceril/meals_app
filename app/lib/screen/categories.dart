import 'package:app/data/dummy_data.dart';
import 'package:app/models/category.dart';
import 'package:app/screen/meals.dart';
import 'package:app/widgets/category_grid_item.dart';
import 'package:flutter/material.dart';

// base screen setup with scaffold
class CategoriesScreen extends StatelessWidget {
  const CategoriesScreen({super.key});

  void _selectCategory(BuildContext context, Categorie catego) {
    final filtermeals = dummyMeals
        .where((meal) => meal.categories.contains(catego.id))
        .toList();

    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => MealsScreen(
        title: catego.title,
        meals: filtermeals,
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pick your Categories'),
      ),
      body: GridView(
          padding: const EdgeInsets.all(20),
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              childAspectRatio: 3 / 2,
              crossAxisSpacing: 20,
              mainAxisSpacing: 20),
          children: availableCategories
              .map(
                (category) => CategoryGridItem(
                  category: category,
                  onSelectCategory: () {
                    _selectCategory(context, category);
                  },
                ),
              )
              .toList()),
    );
  }
}
